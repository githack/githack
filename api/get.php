<?php
//CREATE THE JSON LAYOUT BY THE $result VARIABLE
function show(){

    //CONNECT TO DATABASE
    $con = mysqli_connect('localhost','root','root','hack_db');
    $con->set_charset("utf8");

    //CHECK IF CONNECTION GET ANY ERROR AND SHOW THIS
    if ($con->connect_error) die('{"response":"'.$con->connect_error.'"}');
    else{
        //IF CONNECTION WORK ANYWAY
        if($con){
            $result = mysqli_query($con, "SELECT * FROM `hack_table` ORDER BY `id` DESC");
            
            if($result->num_rows != 0) {
                return $result;
            }else echo '{"response": "empty_db"}';
        }else echo '{"response":"not_database_connection"}';
    }
}
?>