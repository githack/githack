-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 05-Maio-2019 às 16:00
-- Versão do servidor: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `hack_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `hack_table`
--

CREATE TABLE `hack_table` (
  `id` int(11) NOT NULL,
  `nome` text NOT NULL,
  `idade` text NOT NULL,
  `email` text NOT NULL,
  `talento` text NOT NULL,
  `telefone` text NOT NULL,
  `disponibilidade` text NOT NULL,
  `data` text NOT NULL,
  `hora` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hack_table`
--
ALTER TABLE `hack_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hack_table`
--
ALTER TABLE `hack_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;