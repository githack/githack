    <?php
include '../api/get.php';

echo '
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="icon" href="media/logo.png">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet"> 
    <link rel="stylesheet" href="app.css">
    
    <title>Painel de Controle</title>
  </head>
  <body>
        <nav id="mainNavbar" class="navbar navbar-expand-md bg-light">
            <a class="navbar-brand corNav" href="index.php"><img src="media/logo.png" alt="logo" width="50" height="50"></a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link corNav" href="index.php">Painel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link corNav" href="#">Sobre</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link corNav" href="#">Suporte</a>
                    </li>
                    <li class="justify-content-end nav-item dropdown corNav">
                        <a class="nav-link dropdown-toggle corNav" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ações
                        </a>
                            <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item corNav" href="#">Exportar...</a>
                                <a class="dropdown-item corNav" href="#">Treinar Iara</a>
                    </li>
                </ul>
        </nav>
    
        <!-- <section id="jumbo" class="jumbotrom text-center">
            <h1 class="display-2">IARA</h1>
            <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit, sit?</p>
        </section> -->
        <section id="mainRow" class="container mt-5">
            <p class="lead">Iara >> <em>Painel</em></p>
            <h1 id="tituloPage" class="display-3 text-center">Painel de Controle</h1>
            <h3 class="display-6">Tabela de interações:</h3>
            <div>
                <table id="tabelaDados" class="table table-striped table-dark" data-height="400">
                    <thead id="tbHead">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Idade</th>
                            <th scope="col">Email</th>
                            <th scope="col">Talento</th>
                            <th scope="col">Telefone</th>
                            <th scope="col">Disponibilidade</th>
                            <th scope="col">Data</th>
                            <th scope="col">Hora</th>
                            <th scope="col">Conversão</th>
                        </tr>
                    </thead>
                        <tbody>
                    
';
$data = show();
while($rowData = mysqli_fetch_array($data)){
    echo '<tr>';
    echo '<td scope="col" >'.$rowData["id"].'</td>';
    echo '<td scope="col" >'.$rowData["nome"].'</td>';
    echo '<td scope="col" >'.$rowData["idade"].'</td>';
    echo '<td scope="col" >'.$rowData["email"].'</td>';
    echo '<td scope="col" >'.$rowData["talento"].'</td>';
    echo '<td scope="col" >'.$rowData["telefone"].'</td>';
    echo '<td scope="col" >'.$rowData["disponibilidade"].'</td>';
    echo '<td scope="col" >'.$rowData["data"].'</td>';
    echo '<td scope="col" >'.$rowData["hora"].'</td>'; 
    echo '</tr>';
}
echo '
                    </tbody>
                </table>
            </div>
        </section>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
';

?>